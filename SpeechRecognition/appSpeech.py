import speech_recognition as sr

r = sr.Recognizer()

#Lancer le fichier à la racine du git.
tedx = sr.AudioFile('datasets/TEDx.wav')
with tedx as source:
    #audio = r.record(source, duration=20)
    audio1 = r.record(source, duration=20)
    audio2 = r.record(source, duration=20)

print(r.recognize_google(audio1))
print(r.recognize_google(audio2))

