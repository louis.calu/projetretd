from seamless_communication.models.inference import Translator
import torch


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
translator = Translator(
    "seamlessM4T_large",
    "vocoder_36langs",
    device=device,
    dtype=torch.float16 if "cuda" in device.type else torch.float32,
)

translated_text, _, _ = translator.predict("datasets/TEDx.mp3", "s2tt", 'eng')
print(translated_text)

